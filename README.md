# MuIC
Code for simulating muon+p events.

Cloned from Prof. We Li's Repo: https://github.com/davidlw/pythia8303

Author: Osvaldo Miguel Colin

Original author: Prof. Wei Li

# Requirements
All listed requirements are the versions used to develop this code.
The code may no longer be compatible, with newer versions, but it's good to have a reference.

* Pythia 8.306
* Root 6.24
* Boost 1.40

# How to Run
```bash
# Enter the cloned repository
cd repo

# Create a build directory withing the cloned repository
mkdir build
cd build

# Build the code using cmake
cmake ..
make

# Create a directory for the output
# Useful for not confusing the output with other files
mkdir out
cd out

# Make computer go brrr
../MuICSim

# Please hold while computer goes brrr
```
