#include <any>
#include <map>
#include <string>
#include <thread>
#include <tuple>
#include <vector>

#include "ROOT/TProcessExecutor.hxx"

#include "muic/Constants.h"
#include "muic/Simulation.h"

typedef struct {
    int run_number;
    int n_events;
    std::string output_dir;
    std::string output_ext;
    std::map<std::string, std::any> machine;
    std::map<std::string, std::any> variation;
    std::map<std::string, std::any> fc_set;
} DISRunArgs;

typedef struct {
    int run_number;
    int n_events;
    std::string input_lhef;
    std::string output_dir;
    std::string output_ext;
    std::string base_output_name;
    std::map<std::string, std::any> machine;
    std::map<std::string, std::any> fc_set;
} LHERunArgs;

typedef struct {
    mutable std::atomic_int total;
    mutable std::atomic_int ongoing;
    mutable std::atomic_int done;
} RunStats;

// FORWARD DECLARATIONS BECAUSE CPP IS DUMB
void runDIS();

void runDISFromLHE();

// MAIN
int main() {
    runDIS();
    // runDISFromLHE();

    // RETURN
    return 0;
}

// METHODS
void runDIS() {
    // Run Options
    // User should specify the datasets they want to generate here.
    int n_events = 150000;
    std::string output_dir = "../out/";
    std::string output_ext = "_pythia_150k";

    // std::vector<std::string> run_machines = {"hera", "muic", "muic2", "lhmuc"};
    std::vector<std::string> run_machines = {"muic"};

    std::vector<std::string> run_variations = {"nc_mu"};
//    std::vector<std::string> run_variations = {"nc_mu", "nc_mu_anti", "cc_mu",
//                                               "cc_mu_anti"};

    std::vector<std::string> run_fc_sets = {"hera", "heraQ2_10", "heraQ2_100",
                                            "heraQ2_1k", "heraQ2_10k", "heraQ2_100k"};
    // std::vector<std::string> run_fc_sets = {"heraQ2_100k"};

    // Variations
    std::map<std::string, std::map<std::string, std::any>> variations = {
            {"nc_mu",
                    {{"output", "nc_mu"},
                            {"note", "Neutral current mu-"},
                            {"nc", true},  // GeV
                            {"cc", false}, // GeV
                            {"anti", false}}},
            {"nc_mu_anti",
                    {{"output", "nc_anti_mu"},
                            {"note", "Neutral current mu+"},
                            {"nc", true},  // GeV
                            {"cc", false}, // GeV
                            {"anti", true}}},
            {"cc_mu",
                    {{"output", "cc_mu"},
                            {"note", "Charged current mu-"},
                            {"nc", false}, // GeV
                            {"cc", true},  // GeV
                            {"anti", false}}},
            {"cc_mu_anti",
                    {{"output", "cc_anti_mu"},
                            {"note", "Charged current mu+"},
                            {"nc", false}, // GeV
                            {"cc", true},  // GeV
                            {"anti", true}}},
    };

    // Build run args
    int job_number = 0;

    std::vector<DISRunArgs> run_args;

    for (auto &variation_name: run_variations) {
        for (auto &machine_name: run_machines) {
            for (auto &fc_set_name: run_fc_sets) {
                run_args.push_back({job_number++, n_events, output_dir,
                                    output_ext, machines[machine_name],
                                    variations[variation_name],
                                    fiducial_cut_sets[fc_set_name]});
            }
        }
    }

    std::cout << "Generating " << run_args.size() << " datasets." << std::endl;

    // Define job function
    RunStats run_stats;
    run_stats.total = run_args.size();
    run_stats.ongoing = 0;
    run_stats.done = 0;

    auto run_function = [&run_stats](DISRunArgs run_args) {
        // Update Counters
        run_stats.ongoing++;

        // Get parameters
        auto &run_number = run_args.run_number;
        auto &n_events = run_args.n_events;
        auto &output_dir = run_args.output_dir;
        auto &output_ext = run_args.output_ext;
        auto &machine = run_args.machine;
        auto &variation = run_args.variation;
        auto &fc_set = run_args.fc_set;

        // Build output name
        std::string output_machine_name =
                std::any_cast<const char *>(machine["output"]);
        std::string output_variation_name =
                std::any_cast<const char *>(variation["output"]);
        std::string output_fiducial_cut_set_name =
                std::any_cast<const char *>(fc_set["output"]);
        std::string output_name =
                output_machine_name + "_" + output_variation_name + "_" +
                output_fiducial_cut_set_name + output_ext;

        // Log
        std::cout << "Run " << run_number << " begin." << std::endl;
        std::cout << "Runs Remaining To Process: "
                  << (run_stats.total - run_stats.done - run_stats.ongoing)
                  << std::endl;
        std::cout << "Run " << run_number << " Output: " << output_name
                  << std::endl;

        // Configure
        Simulation simulation;
        simulation.setOutputPath(output_dir + output_name);
        simulation.setNEvents(n_events);
        simulation.setEProton(std::any_cast<double>(machine["eProton"]));
        simulation.setEMuon(std::any_cast<double>(machine["eMuon"]));
        simulation.setQ2Min(std::any_cast<double>(fc_set["Q2Min"]));
        simulation.setYMin(std::any_cast<double>(fc_set["yMin"]));
        simulation.setYMax(std::any_cast<double>(fc_set["yMax"]));
        simulation.setUseNeutralCurrentEn(std::any_cast<bool>(variation["nc"]));
        simulation.setUseChargedCurrentEn(std::any_cast<bool>(variation["cc"]));
        simulation.setUseAntimuonEn(std::any_cast<bool>(variation["anti"]));
//        simulation.setUseColorReconnectionFixEn(true);

        // Run
        simulation.run();

        // Update Counters
        run_stats.ongoing--;
        run_stats.done++;

        // Log
        std::cout << "Run " << run_number << " end." << std::endl;
        std::cout << "Runs Remaining to Complete: "
                  << (run_stats.total - run_stats.done) << std::endl;

        return 0;
    };

    // GO BRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
    auto hardware_concurrency = std::thread::hardware_concurrency();

    std::cout << "Cores " << hardware_concurrency << " that wil go brrrr."
              << std::endl;

    ROOT::TProcessExecutor workers(hardware_concurrency);

    workers.Map(run_function, run_args);
}

void runDISFromLHE() {
    // Run Options
    // User should specify the datasets they want to generate here.
    int n_events = 100000;
    std::string input_dir = "/home/om15/Workspace/lq_test/out/";
    std::string output_dir = input_dir;
    std::string output_ext = "_mg5_100k";

    // Build run args
    int job_number = 0;
    std::vector<LHERunArgs> run_args;

    std::string input_lhef = input_dir + "lq_b_mu_scat/Events/MuIC_mz_500GeV/unweighted_events.lhe.gz";
    std::string output_base_name = "MuIC_mz_500GeV";

    std::cout << "Queued input: " << input_lhef << std::endl;

    run_args.push_back({
                               job_number++, n_events, input_lhef,
                               output_dir, output_ext, output_base_name,
                               machines["muic"], fiducial_cut_sets["heraQ2_100"]
                       });

    std::cout << "Generating " << run_args.size() << " datasets." << std::endl;

    // DEFINE JOB FUNCTION
    auto run_stats = RunStats();
    run_stats.total = run_args.size();
    run_stats.ongoing = 0;
    run_stats.done = 0;

    auto run_function = [&run_stats](LHERunArgs args) {
        // Get & Update Counters
        run_stats.ongoing++;

        // Get parameters
        auto &run_number = args.run_number;
        auto &n_events = args.n_events;
        auto &output_dir = args.output_dir;
        auto &output_ext = args.output_ext;
        auto &machine = args.machine;
        auto &fc_set = args.fc_set;
        auto &input_lhef = args.input_lhef;
        auto &base_output_name = args.base_output_name;

        // Build output name
        std::string output_fiducial_cut_set_name =
                std::any_cast<const char *>(fc_set["output"]);
        std::string output_name = base_output_name + "_" +
                                  output_fiducial_cut_set_name + output_ext +
                                  ".root";

        // Log
        std::cout << "Run " << run_number << " begin." << std::endl;
        std::cout << "Runs Remaining To Process: "
                  << (run_stats.total - run_stats.done - run_stats.ongoing)
                  << std::endl;
        std::cout << "Run " << run_number << " Output: " << output_name
                  << std::endl;

        // Configure
        Simulation simulation;
        simulation.setOutputPath(output_dir + output_name);
        simulation.setNEvents(n_events);
        simulation.setLHEInputFile(input_lhef);
        simulation.setUseColorReconnectionFixEn(false);
        simulation.setEProton(std::any_cast<double>(machine["eProton"]));
        simulation.setEMuon(std::any_cast<double>(machine["eMuon"]));
        simulation.setQ2Min(std::any_cast<double>(fc_set["Q2Min"]));
        simulation.setYMin(std::any_cast<double>(fc_set["yMin"]));
        simulation.setYMax(std::any_cast<double>(fc_set["yMax"]));
        simulation.setUseColorReconnectionFixEn(true);

        // Run
        simulation.run();

        // Update Counters
        run_stats.ongoing--;
        run_stats.done++;

        // Log
        std::cout << "Run " << run_number << " end." << std::endl;
        std::cout << "Runs Remaining to Complete: "
                  << (run_stats.total - run_stats.done) << std::endl;

        return 0;
    };

    // GO BRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR
    auto hardware_concurrency = std::thread::hardware_concurrency();

    std::cout << "Cores " << hardware_concurrency << " that wil go brrrr."
              << std::endl;

    ROOT::TProcessExecutor workers(hardware_concurrency);

    workers.Map(run_function, run_args);
}
