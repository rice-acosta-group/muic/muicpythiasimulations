//
// Created by omiguelc on 4/6/22.
//

#ifndef MUICPYTHIASIMULATIONS_CONSTANTS_H
#define MUICPYTHIASIMULATIONS_CONSTANTS_H

#include <any>
#include <string>

// Machines
std::map<std::string, std::map<std::string, std::any>> machines = {
        {"muic",
                {{"output", "muic"},
                        {"note", "For MuIC analysis"},
                        {"eProton", 275.}, // GeV
                        {"eMuon", 960.}}},
        {"muic2",
                {{"output", "muic2"},
                        {"note", "For MuIC analysis"},
                        {"eProton", 1000.}, // GeV
                        {"eMuon", 1000.}}},
        {"lhmuc",
                {{"output", "lhmuc"},
                        {"note", "For MuIC analysis"},
                        {"eProton", 7000.}, // GeV
                        {"eMuon", 1500.}}},
        {"hera",
                {{"output", "hera"},
                        {"note", "For comparing with HERA"},
                        {"eProton", 920.}, // GeV
                        {"eMuon", 27.5}}},
};

// FIDUCIAL CUTS
std::map<std::string, std::map<std::string, std::any>> fiducial_cut_sets = {
        {"nocuts",
                {{"output", ""},
                        {"note", "No cuts"},
                        {"Q2Min", 0}, // GeV
                        {"yMin", 0},
                        {"yMax", 1}}},
        {"legacy",
                {{"output", "q2_1_ymin_0p001_ymax_0p999"},
                        {"note", "Used by Dr. Wei Li"},
                        {"Q2Min", 1.}, // GeV
                        {"yMin", 0.001},
                        {"yMax", 0.999}}},
        {"main",
                {{"output", "q2_1_ymin_0p01_ymax_0p99"},
                        {"note", "For main analysis"},
                        {"Q2Min", 1.}, // GeV
                        {"yMin", 0.01},
                        {"yMax", 0.99}}},
        {"hera",
                {{"output", "q2_1_ymin_0p01_ymax_0p9"},
                        {"note", "For hera comparison"},
                        {"Q2Min", 1.}, // GeV
                        {"yMin", 0.01},
                        {"yMax", 0.9}}},
        {"heraQ2_10",
                {{"output", "q2_10_ymin_0p01_ymax_0p9"},
                        {"note", "For hera comparison @ Q2 > 10 GeV^2"},
                        {"Q2Min", 10.}, // GeV
                        {"yMin", 0.01},
                        {"yMax", 0.9}}},
        {"heraQ2_100",
                {{"output", "q2_100_ymin_0p01_ymax_0p9"},
                        {"note", "For hera comparison @ Q2 > 100 GeV^2"},
                        {"Q2Min", 100.}, // GeV
                        {"yMin", 0.01},
                        {"yMax", 0.9}}},
        {"heraQ2_1k",
                {{"output", "q2_1k_ymin_0p01_ymax_0p9"},
                        {"note", "For hera comparison @ Q2 > 1k GeV^2"},
                        {"Q2Min", 1e3}, // GeV
                        {"yMin", 0.01},
                        {"yMax", 0.9}}},
        {"heraQ2_10k",
                {{"output", "q2_10k_ymin_0p01_ymax_0p9"},
                        {"note", "For hera comparison @ Q2 > 10k GeV^2"},
                        {"Q2Min", 10e3}, // GeV
                        {"yMin", 0.01},
                        {"yMax", 0.9}}},
        {"heraQ2_100k",
                {{"output", "q2_100k_ymin_0p01_ymax_0p9"},
                        {"note", "For hera comparison @ Q2 > 100k GeV^2"},
                        {"Q2Min", 100e3}, // GeV
                        {"yMin", 0.01},
                        {"yMax", 0.9}}},
        {"heraQ2_1M",
                {{"output", "q2_1M_ymin_0p01_ymax_0p9"},
                        {"note", "For hera comparison @ Q2 > 1M GeV^2"},
                        {"Q2Min", 1e6}, // GeV
                        {"yMin", 0.01},
                        {"yMax", 0.9}}},
};

#endif // MUICPYTHIASIMULATIONS_CONSTANTS_H
