//
// Created by omiguelc on 09/01/22.
//

#ifndef MUICPYTHIASIMULATIONS_NTUPLES_H
#define MUICPYTHIASIMULATIONS_NTUPLES_H

#include <vector>

#include "TTree.h"

using namespace std;

class OutputNtuple {
public:
    OutputNtuple();

    ~OutputNtuple();

    double evt_weight_;

    double Q2_, W2_, x_, y_;
    double rec_Q2_, rec_x_, rec_y_, rec_agljb_;
    double mu_e_, mu_pt_, mu_phi_, mu_eta_;
    double mu_sm_pt_, mu_sm_phi_, mu_sm_eta_;

    int vsize_had_;
    vector<int> had_pdgid_;
    vector<double> had_e_, had_pt_, had_phi_, had_eta_, had_mass_;
    vector<double> had_sm_pt_, had_sm_phi_, had_sm_eta_;

    void registerBranches(TTree *tree);

    void clear();
};

#endif // MUICPYTHIASIMULATIONS_NTUPLES_H
